#What is the result of

#  7/2
def one():
    print("\n7/2")
    print(7 / 2)

#  7//2  
def two():
    print("\n7//2")
    print(7 // 2)

def three():
    print("\n-7/2")
    print(-7 / 2)

def four():
    print("\n-7//2")
    print(-7 // 2)


one()
two()
three()
four()
