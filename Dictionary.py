#1.) Write a function to find the number of occurrences of each word in a string(Don't consider punctuation characters)
def one():
    print("\nWrite a function to find the number of occurrences of each word in a string(Don't consider punctuation characters)\n")
    #s=""
    s="Python is an interpreted, high-level, general-purpose programming language. Python interpreters are available for many operating systems.Python is a multi-paradigm programming language. Object-oriented programming and structured programming are fully supported."
    dict = {}
    count = 0
    s = s.replace(".","")
    if s is "":
        print("{}")
    else:
        for w in s.split():
            if w in dict:
                count = dict.get(w)
                count+=1
                dict[w] = count
            else:
                dict[w] = 1
        print(dict)   

#2.) Define a function that prints all the items (keys, values) in a dictionary
def two():
    d={'c': 2, 'b': 3, 'a': 1}
    print_dict_keys_and_values(d)

def print_dict_keys_and_values(d):
    print("\nDefine a function that prints all the items (keys, values) in a dictionary")
    for k in d:
        print(k, d.get(k))

#3.) Define a function that returns a list of all the items in a dictionary sorted by the dictionary's keys.

def three():
    print("\nDefine a function that returns a list of all the items in a dictionary sorted by the dictionary's keys.")
    d={'c': 2, 'b': 3, 'a': 1}  
    print(d.items())     

one() 
two()  
three() 
