# x = [1, 2, 3, 4]

#1.) Print all the elements in x
def one():
    x = [1, 2, 3, 4]
    print("Print all the elements in x,  where x =",x)
    for i in x:
        print(i)

#2.) Print all the elements and their indexes using the enumerate function
def two():
    x = [1, 2, 3, 4]
    print("Print all the elements and their indexes using the enumerate function")
    for i,v in enumerate(x):
        print(i,v)

#3.) Print all the integers from 10 to 0 in descending order using range
def three():
    x = [1, 2, 3, 4]
    print("Print all the integers from 10 to 0 in descending order using range")
    for i in range(len(x)-1,-1,-1):
        print(x[i])  

#4.) Print all the integers from 10 to 0 in descending order using while
def four():
    x = [1, 2, 3, 4]
    print("Print all the integers from 10 to 0 in descending order using while")
    i = len(x)-1
    while i >= 0:
        print(x[i])
        i = i - 1

one()
two()
three()
four()
