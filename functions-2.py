#1.) Define a function, args_sum that accepts an arbitrary number of integers as input and computes their sum.
#    Modify the args_sum function so that it accepts an optional, boolean, keyword argument named absolute. 
#    If absolute is True, then args_sum must return the absolute value of the sum of *args. If absolute is not 
#     specified, then return the sum without performing any conversion.

def one():
    print("Define a function, args_sum that accepts an arbitrary number of integers as input and computes their sum.")
    args_sum(4.2,-5,absolute=True)

def args_sum(*args,**kwargs):
    for k,v in kwargs.items():
        absolute = v    
    summ=0
    for num in args:
        summ+=num
    if absolute is True:
        print(abs(summ)) 
    else:
        print(summ)
    
one()
