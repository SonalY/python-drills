# x = "one two three four"

#1.) Print the last 3 characters of x
def one():
    x = "one two three four"
    print("\nPrint the last 3 characters of x,  where x = ",x)
    print(x[-3:len(x)])

#2.) Print the first 10 characters of x
def two():
    x = "one two three four"
    print("\nPrint the first 10 characters of x")
    print(x[0:10])

#3.) Print characetrs 4 through 10 of x
def three():
    x = "one two three four"
    print("\nPrint characetrs 4 through 10 of x")
    print(x[4:10])

#4.) Find the length of x
def four():
    x = "one two three four"
    print("\nFind the length of x")
    print(len(x))

#5.) Split x into its words
def five():
    x = "one two three four"
    print("\nSplit x into its words")
    print(x.split(" "))

#6.) Capitalize the first character of x
def six():
    x = "one two three four"
    print("\nCapitalize the first character of x")
    print(x.capitalize())

#7.) Convert x into uppercase
def seven():
    x = "one two three four"
    print("\nConvert x into uppercase")
    print(x.upper())

#8.) x = "one two three four"
#    y = x
#    x = "one two three"
#    After executing the above code, what is the value of y?
def eight():
    x = "one two three four"
    y = x
    x = "one two three"
    print("\nx = \"one two three four\" \ny = x \nx = \"one two three\"\nAfter executing the above code, what is the value of y?")
    print("y =",y)


one()
two()
three()
four()
five()
six()
seven()
eight()
