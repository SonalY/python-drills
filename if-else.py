'''x = 1
y = 10
if x > y:
    x = y
else:
    y = x + 1
What is y after the above code is executed?
'''
def one():
    print("\nx = 1\ny = 10\nif x > y:\n  x = y\nelse:\n  y = x + 1\nWhat is y after the above code is executed?")
    x = 1
    y = 10
    if x > y:
        x = y
    else:
        y = x + 1
    print("y =",y)  

one()

