# x = [3, 1, 2, 4, 1, 2]

#1.) Find the sum of all the elements in x
def one():
    x = [3, 1, 2, 4, 1, 2]
    print("\nFind the sum of all the elements in x,   where x =",x)
    print(sum(x))

#2.) Find the length of x
def two():
    x = [3, 1, 2, 4, 1, 2]
    print("\nFind the length of x")
    print(len(x))

#3.) Find the length of x
def three():
    x = [3, 1, 2, 4, 1, 2]
    print("\nPrint the last three items of x")
    print(x[-3:len(x)])

#4.) Find the length of x
def four():
    x = [3, 1, 2, 4, 1, 2]
    print("\nPrint the first three items of x")
    print(x[0:3])

#5.) Sort x
def five():
    x = [3, 1, 2, 4, 1, 2]
    print("\nSort x")
    print(sorted(x))    

#6.) Add another item, 10 to x
def six():
    x = [3, 1, 2, 4, 1, 2]
    print("\nAdd another item, 10 to x")
    x.append(10)
    print(x)

#7.) Add another item 11 to x
def seven():
    x = [3, 1, 2, 4, 1, 2, 10]
    print("\nAdd another item 11 to x")
    x.append(11)
    print(x)

#8.) Remove the last item from x
def eight():
    x = [3, 1, 2, 4, 1, 2, 10, 11]
    print("\nRemove the last item from x")
    x.pop()
    print(x)

#9.) How many times does 1 occur in x?
def nine():
    x = [3, 1, 2, 4, 1, 2, 10]
    print("\nHow many times does 1 occur in x?")
    print(x.count(1))

#10.) Check whether the element 10 is present in x
def ten():
    x = [3, 1, 2, 4, 1, 2, 10]
    print("\nCheck whether the element 10 is present in x")
    if 10 in x:
        print("Yes")
    else:
        print("No")  

# y = [5, 1, 2, 3]
#11.) Add all the elements of y to x
def eleven():
    x = [3, 1, 2, 4, 1, 2, 10]
    y = [5, 1, 2, 3]
    print("\nAdd all the elements of y to x ,  where y =", y)
    x=x+y
    print(x)

#12.) Create a copy of x
def twelve():
    x = [3, 1, 2, 4, 1, 2, 10]
    print("\nCreate a copy of x")
    y=x
    print("y = x\ny =", y)

#13.) Can a list contain elements of different data-types?
def thirteen():
    x = [3, "a", 2, "palm trees", 1, 2.7, True]
    print("\nCan a list contain elements of different data-types?")
    print("This is a list =",x)    

#14.) Which data structure does Python use to implement a list?
def fourteen():
    print("\nData structure used in Python is ARRAYS")
    

one()
two()
three()
four()
five()
six()
seven()
eight()
nine()
ten()
eleven()
twelve()
thirteen()
fourteen()