'''Create a class called Point which has two instance variables, x and y that represent the x and y co-ordinates 
respectively. Initialize these instance variables in the __init__ method

Define a method, distance on Point which accepts another Point object as an argument and returns the distance between 
the two points '''
import math

class Point:

    def __init__(self,x,y):
        self.x=x
        self.y=y

    def distance(self,ob):
        d=math.sqrt((self.x - ob.x )**2 + (self.y - ob.y)**2 )
        print(d)


point_obj=Point(2,4)
point_obj2=Point(1,2)
point_obj.distance(point_obj2)