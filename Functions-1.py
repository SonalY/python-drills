# Write a function that accepts an integer n, n > 0, and returns a list of all n-digit prime numbers. 
# Did you write a helper function? If not, write a helper function, is_prime that returns whether a number is 
# prime or not, and use it in your n_digit_primes function


def n_digit_primes(n):
    print(n,"-digit prime numbers")
    primes = []
    if n>0:
        dic = {1:[2,9],2:[10,99],3:[100,999],4:[1000,9999],5:[10000,99999]}
        if n in dic:
            primes = is_prime(dic.get(n))
            print(primes)
        else:
            print("Sorry the number got too big!")

def is_prime(num):
    ls_prime = []
    flag = False
    x = num[0]
    r = num[1]
    while x <= r:
        flag = False
        for i in range(2,x//2):
            if(x%i == 0):
                flag = True
                break
            else:
                continue
        if flag is False:
            ls_prime.append(x)
        x+=1
    return ls_prime    
        
def user_interact():
    n_digit_primes(n = 2)
    n_digit_primes(n = 1)


user_interact()